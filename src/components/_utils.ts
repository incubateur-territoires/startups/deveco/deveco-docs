import type { Page } from "../types";

export function breadcrumbBuild({
  pages,
  id,
  rootId,
  rootTitle,
}: {
  pages: Page[];
  id: string;
  rootId: string;
  rootTitle: string;
}) {
  const breadcrumb = [
    {
      url: `/${rootId}`,
      id: rootId,
      title: rootTitle,
    },
  ] as Page[];

  if (id === rootId) {
    return breadcrumb;
  }

  const page = pages.find((p) => p.id === id);

  if (!page) {
    return breadcrumb;
  }

  const { pages: childrenPages, ...pageWithoutPages } = page;

  const dir = {
    ...pageWithoutPages,
    url: `/rootId/${id}`,
  };

  breadcrumb.push(dir);

  return breadcrumb;
}
const mode = import.meta.env.MODE;

const directusAccessToken =
  mode === "production"
    ? process.env.DIRECTUS_ACCESS_TOKEN
    : import.meta.env.DIRECTUS_ACCESS_TOKEN;
const directusUrl =
  mode === "production"
    ? process.env.DIRECTUS_URL
    : import.meta.env.DIRECTUS_URL;

console.log(directusUrl);

export async function directusGet({
  section,
  id,
}: {
  section: string;
  id?: string;
}) {
  const url = id
    ? `${directusUrl}/items/${section}/${id}?filter[status][_eq]=published`
    : `${directusUrl}/items/${section}?filter[status][_eq]=published`;

  const response = await fetch(url, {
    method: "get",
    headers: {
      Authorization: `Bearer ${directusAccessToken}`,
    },
  });

  const json = await response.json();
  return json.data;
}

export function unescape(text: string) {
  return text
    .replaceAll("&amp;", "&")
    .replaceAll("&lt;", "<")
    .replaceAll("&gt;", ">")
    .replaceAll("&quot;", '"')
    .replaceAll("&#039;", "'")
    .replaceAll("&#39;", "'")
    .replaceAll("&#038;", "'")
    .replaceAll("&#38;#39;", "'")
    .replace(/(\r\n|\n|\r)/gm, " ");
}

const block = (text: string) => text + "\n\n";
// const escapeBlock = (text: string) => escape(text) + "\n\n";
const line = (text: string) => text + "\n";
const inline = (text: string) => text;
const newline = () => "\n";
const empty = () => "";

export const markdownToTxtRender = {
  // Block elements
  code: block,
  blockquote: block,
  html: empty,
  heading: block,
  hr: newline,
  list: (text: string) => block(text.trim()),
  listitem: line,
  checkbox: empty,
  paragraph: block,
  table: (header: string, body: string) => line(header + body),
  tablerow: (text: string) => line(text.trim()),
  tablecell: (text: string) => text + " ",
  // Inline elements
  strong: inline,
  em: inline,
  codespan: inline,
  br: newline,
  del: inline,
  link: (_0, _1, text: string) => text,
  image: (_0, _1, text: string) => text,
  text: inline,
  // etc.
  options: {},
};
