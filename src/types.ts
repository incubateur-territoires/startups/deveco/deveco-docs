export type Page = {
  id: string;
  url?: string;
  title?: string;
  section?: string;
  position?: number;
  pages?: Page[];
};