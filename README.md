# Deveco docs

Ce repo héberge le site de contenu de deveco.

Le contenu lui même provient de directus sur l'instance de l'incubateur (https://directus.incubateur.anct.gouv.fr).

Variables d'environnement:

```env
DIRECTUS_ACCESS_TOKEN="the-token"
DIRECTUS_URL="https://directus.incubateur.anct.gouv.fr"
```

Une configuration docker démarre l'application sur un serveur node et expose le port `4321`.
