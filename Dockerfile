FROM node:20 AS deps
WORKDIR /app

COPY package.json package-lock.json ./
RUN ls
RUN npm ci --omit=dev

FROM deps AS build

RUN npm ci
COPY . ./
RUN npm run build

FROM gcr.io/distroless/nodejs20-debian11

WORKDIR /app
COPY --from=deps /app/node_modules/ /app/node_modules/
COPY --from=build /app/dist/ /app/dist/

ENV HOST=0.0.0.0
ENV PORT=4321
EXPOSE 4321

CMD ["./dist/server/entry.mjs"]
